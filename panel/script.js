document.addEventListener('DOMContentLoaded', function() {
    const form = document.getElementById('myForm');
  
    form.addEventListener('submit', function(event) {
      event.preventDefault(); // Prevent the form from submitting
  
      const formData = new FormData(form);
      const textValue = formData.get('text');
  
      const requestOptions = {
        method: 'POST',
        body: JSON.stringify({ text: textValue }),
        headers: {
          'Content-Type': 'application/json'
        }
      };
  
      fetch('/submit', requestOptions)
        .then(response => response.json())
        .then(data => {
          console.log('Response:', data);
          // Do something with the response data
        })
        .catch(error => {
          console.error('Error:', error);
          // Handle the error
        });
    });
  });
  