function postIt() {
    var e = document.getElementById("method");
    var value = e.value;

    const url = 'http://127.0.0.1:8000/submit';

    const arguments = {
        target: document.getElementById("target").value,
        port: document.getElementById("port").value,
        time: document.getElementById("time").value,
        concurrents: document.getElementById("concurrents").value,
        method: document.getElementById("method").value
    };

    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(arguments),
    };

    fetch(url, options)
        .then(response => response.json())
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.error('Error:', error);
        });

}